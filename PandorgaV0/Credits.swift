import SpriteKit
import GameKit

class Credits: SKScene{
	
	// BUTTONS
	var goBack: SKSpriteNode!
	
	override func didMove(to view: SKView) {
		// BUTTONS
		goBack = childNode(withName: "btnBack") as! SKSpriteNode
		
		SoundManager.main.play(sound: .runner)
		
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			
			if ( goBack.contains(location) ){

				if let scene = MainScreen(fileNamed: "MainScreen") {
					if let sceneNode = scene as! MainScreen? {
						let transition = SKTransition.crossFade(withDuration: 0.5)
						sceneNode.scaleMode = .aspectFill
						if let view = self.view as! SKView? {
							view.presentScene(sceneNode, transition: transition)
							view.ignoresSiblingOrder = false
							view.showsFPS = false
							view.showsNodeCount = false
						}
					}
				}

				//				let reveal = SKTransition.crossFade(withDuration: 0.5)
//				let newScene = SKScene(fileNamed: "MainScreen")
//				if let view = self.view as! SKView? {
//					view.presentScene(newScene!, transition: reveal)
//				}
			}
		}
	}

	
}

