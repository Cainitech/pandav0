import SpriteKit
import GameKit

class MainScreen: SKScene{
	
	// BUTTONS
	var playHardCore: SKSpriteNode!
	var playNormal: SKSpriteNode!
	var credits: SKSpriteNode!
	var pokedex: SKSpriteNode!
	
	var loadingSprite = SKSpriteNode()
	
	fileprivate var nextScene: SKScene?

	override func didMove(to view: SKView) {
		// BUTTONS
		playHardCore = childNode(withName: "playHardCore") as! SKSpriteNode
		playNormal = childNode(withName: "playNormal") as! SKSpriteNode
		credits = childNode(withName: "credits") as! SKSpriteNode
		pokedex = childNode(withName: "pokedex") as! SKSpriteNode

        SoundManager.main.play(sound: .intro)
		
		loadingSprite = SKSpriteNode(imageNamed: "obj_002")
		loadingSprite.size = CGSize(width: 100, height: 100)
		loadingSprite.position = CGPoint(x: 0, y: 0)

//		preloadGameScene()
	}
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            if playHardCore.contains(location) {
                playHardCore.texture = SKTexture(imageNamed: "finalButton02Pressed")
            } else if playNormal.contains(location) {
                playNormal.texture = SKTexture(imageNamed: "finalButton02Pressed")
            } else if pokedex.contains(location) {
                pokedex.texture = SKTexture(imageNamed: "finalButton02Pressed")
            } else if credits.contains(location) {
                credits.texture = SKTexture(imageNamed: "finalButton02Pressed")
            }
        }
    }
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		for touch in touches {
			let location = touch.location(in: self)
			
			if ( playHardCore.contains(location) ){
				//Mudar tela para gamescene
				goToGameScene()
//                let teste = SKAction.playSoundFileNamed(String, waitForCompletion: false)
			} else
				if ( playNormal.contains(location) ){
					//Mudar tela para gamescene
					goToGameScene()
					
				} else
					if ( credits.contains(location) ){
						//Mudar tela para gamescene
						goCredits()
					} else
						if ( pokedex.contains(location) ){
							//Mudar tela para gamescene
							
                            //performSegue
                            
                            //No retorno daquela segue vai ter que ter uma segue de retorno para a gameviewcontroller
						}

		}
	}
	
	func doLoading(){
		self.addChild(loadingSprite)
		loadingSprite.run(SKAction.repeatForever(SKAction.rotate(byAngle: 3.0, duration: 0.5) ))

	}
	
	func stopLoading(){
		loadingSprite.removeFromParent()
	}
	
	func goCredits(){
		if let scene = Credits(fileNamed: "Credits") {
			if let sceneNode = scene as! Credits? {
				let transition = SKTransition.crossFade(withDuration: 0.5)
				sceneNode.scaleMode = .aspectFill
				if let view = self.view as! SKView? {
					view.presentScene(sceneNode, transition: transition)
					view.ignoresSiblingOrder = false
					view.showsFPS = false
					view.showsNodeCount = false
				}
			}
		}

//		let reveal = SKTransition.crossFade(withDuration: 0.5)
//		let newScene = SKScene(fileNamed: "Credits")
//		if let view = self.view {
//			view.presentScene(newScene!, transition: reveal)
//		}
	}
	
	func goToGameScene () {
		if let scene = GameScene(fileNamed: "GameScene") {
			if let sceneNode = scene as! GameScene? {
				let transition = SKTransition.crossFade(withDuration: 0.5)
				sceneNode.scaleMode = .aspectFill
				if let view = self.view as! SKView? {
					view.presentScene(sceneNode, transition: transition)
					view.ignoresSiblingOrder = false
					view.showsFPS = false
					view.showsNodeCount = false
				}
			}
		}
		
//		guard let nextScene = nextScene else {return}
//		let transition = SKTransition.crossFade(withDuration: 1.0)
//		nextScene.scaleMode = .aspectFill
//		scene!.view?.presentScene(nextScene, transition: transition)
	}


	func preloadGameScene () {
		let qualityOfServiceClass = DispatchQoS.QoSClass.background
		let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
		backgroundQueue.async(execute: {
			self.nextScene = GameScene(fileNamed:"GameScene")
//			DispatchQueue.main.async(execute: { () -> Void in
//			})
		})
	}
}

