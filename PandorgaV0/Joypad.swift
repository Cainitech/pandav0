import SpriteKit
import GameKit

class Joypad {
	
	let axis: SKSpriteNode!
	let dir: SKSpriteNode!
	let scene: SKScene!
	let transitionTime = 0.3
	var dispX:CGFloat = 0.0
	var lastMove: CGFloat = 0.0

	init(_ scene: SKScene){
		self.scene = scene
		
		dir = SKSpriteNode(imageNamed: "padball")
		dir.size = CGSize(width: 120.0, height: 120.0)
		dir.anchorPoint = CGPoint(x: 0.5, y: 0.5)
		dir.position = CGPoint(x: -500.0, y: -220.0)
		dir.alpha = 0.0
		scene.addChild(dir)
		
		axis = SKSpriteNode(imageNamed: "padspot")
		axis.size = CGSize(width: 250.0, height: 250.0)
		axis.anchorPoint = CGPoint(x: 0.5, y: 0.5)
		axis.position = CGPoint(x: -500.0, y: -220.0)
		axis.alpha = 0.0
		scene.addChild(axis)
		
	}
	
	func setStart( _ place: CGPoint ){
		dir.position = place
		axis.position = place
	}
	
	func moveCtrl( _ place: CGPoint ){
		let maxDist = axis.size.width * 0.4

		let deltaX = abs( place.x - axis.position.x )
		if deltaX < maxDist {
			dir.position.x = place.x
		} else {
			let diffX = deltaX - maxDist
			let newPosX = place.x > dir.position.x ? place.x - diffX : place.x + diffX
			dir.position.x = newPosX
		}
		lastMove = dispX
		dispX = ( dir.position.x - axis.position.x ) / maxDist

		let deltaY = abs( place.y - axis.position.y )
		if deltaY < maxDist {
			dir.position.y = place.y
		} else {
			let diffY = deltaY - maxDist
			let newPosY = place.y > dir.position.y ? place.y - diffY : place.y + diffY
			dir.position.y = newPosY
		}

	
	}
	
	func moveXby(_ diffX: CGFloat){
		dir.position.x = dir.position.x - diffX
		axis.position.x = axis.position.x - diffX
	}
	
	func show(){
		dir.run(SKAction.fadeAlpha(to: 0.7, duration: transitionTime))
		axis.run(SKAction.fadeAlpha(to: 0.4, duration: transitionTime))
	}
	
	func hide(){
		dir.run(SKAction.fadeOut(withDuration: transitionTime))
		axis.run(SKAction.fadeOut(withDuration: transitionTime))
		lastMove = dispX
		dispX = 0.0
	}
	
}
