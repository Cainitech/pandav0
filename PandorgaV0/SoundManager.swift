//
//  SoundManager.swift
//  PandorgaV0
//
//  Created by Rafael Avelino do Prado Garcia Ribeiro on 14/11/17.
//  Copyright © 2017 Caio Felipe Giasson. All rights reserved.
//

import AVFoundation
import SpriteKit

enum Sound: String {
    
    // Soundtrack
	case intro = "soundtrack_intro.mp3"
	case intro_alt = "soundtrack_intro_alt.mp3"
    case cub = "soundtrack_cub.mp3"
    case mom = "soundtrack_mom.mp3"
    case runner = "soundtrack_runner.mp3"
	
    // SFX
    case dyingBee = "dyingBee.wav"
    case bee = "bee.wav"
    case flapingWings = "flapingWings.wav"
    case runningWater = "runningWater.wav"
    case heavyRain = "heavyRain.wav"
    case lightRain = "lightRain.wav"
    case rustle = "rustle.wav"
    case forest = "forest.wav"
    case strongWind = "strongWind.wav"
    case lightWind = "lightWind.wav"
    case dyingWolf = "dyingWolf.wav"
    case howlingWolf = "howlingWolf.wav"
    case dyingMonkey = "dyingMonkey.wav"
    case voicesMonkeys = "voicesMonkeys.wav"
    case voiceMonkey01 = "voiceMonkey01.wav"
    case voiceMonkey02 = "voiceMonkey02.wav"
    case dragObject = "dragObjec.wav"
    case fallObject01 = "fallObject01.wav"
    case fallObject02 = "fallObject02.wav"
    case breakObject01 = "breakObject01.wav"
    case breakObject02 = "breakObject02.wav"
    case breakObject03 = "breakObject03.wav"
    case grabCub = "grabCub.wav"
    case jumpCub = "jumpCub.wav"
    case pushPullCub = "pushPullCub.wav"
    case attackMom = "attackMom.wav"
    case jumpMom = "jumpMom.wav"
    case roarMom01 = "roarMom01.wav"
    case roarMom02 = "roarMom02.wav"
    case systemSFX01 = "systemSFX01.wav"
    case systemSFX02 = "systemSFX02.wav"
    case systemSFX03 = "systemSFX03.wav"
    case dyingTiger = "dyingTiger.wav"
    
    static let soundtracks: [Sound] = [intro, cub, mom, runner]
    static let sfx: [Sound] = [dyingBee, bee, flapingWings, runningWater, heavyRain, lightRain, rustle, forest, strongWind, lightWind, dyingWolf, howlingWolf, dyingMonkey, voicesMonkeys, voiceMonkey01, voiceMonkey02, dragObject, fallObject01, fallObject02, breakObject01, breakObject02, breakObject03, grabCub, jumpCub, pushPullCub, attackMom, jumpMom, roarMom01, roarMom02, systemSFX01, systemSFX02, systemSFX03, dyingTiger]
    
    static let all: [Sound] = Sound.soundtracks + Sound.sfx
}

class SoundManager {
    static let main = SoundManager()
    
    var audioPlayers = [Sound: AVAudioPlayer]()
	var currentSound: Sound?
	
    fileprivate init() {
		print("JFJFJFJFJFJFJFJFJFJFJFJF")
        for sound in Sound.all {
			guard let path = Bundle.main.path(forResource: sound.rawValue, ofType: nil) else { return }
			print( path )
            let url = URL(fileURLWithPath: path)
            var audioPlayer: AVAudioPlayer
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
				audioPlayer.prepareToPlay()
                audioPlayers[sound] = audioPlayer
            } catch { continue }
        }
    }
    
    func play(sound: Sound) {
		if currentSound != sound {
			stop()
			audioPlayers[sound]?.play()
			currentSound = sound
		}
    }
//    func play(sfx: Sound) {
//        SKAction.playSoundFileNamed(sfx.rawValue, waitForCompletion: false)
//    }
	
	func stop() {
		guard let cs = currentSound else { return }
		audioPlayers[cs]?.stop()
		audioPlayers[cs]?.currentTime = 0
	}

	
}
