import SpriteKit
import GameplayKit

class Borboleta: SKSpriteNode{
	
	var standTexture = SKTexture()
	
	var isStanding = false
	
	required init?(coder aDecoder: NSCoder){
		super.init(coder: aDecoder)
	
		standTexture = SKTexture(imageNamed: "voando7")
	}

	func stand( at: CGPoint ){
		self.position = at
		
	}
	
	func fly( to: CGPoint ){
		self.position = to
	}
}
