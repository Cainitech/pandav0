
import UIKit
import SpriteKit

class Panda: SKSpriteNode {
	
	// Identifier (Mother... Child...)
	var who: String!

	// Physics and movement stuff
	var walkDirection = 0.0
	var walkInterval = 0.1
	var isWalking = false
	// JUMP flags
	var lastPosition = CGPoint()
	var isJumping = false
	var isHolding = false
	var isGrounded = false
	
	// Character specyfic data. Will change when switching between child and mother
	var baseWalkForce = 2000.0
	var baseJumpStrength = 20000.0
	var baseMass = 20.0
	var baseMassHigh = 80.0
	var baseLinearDamping = 25
	var baseFriction = 0.1
	var baseXscale = 1.0
	var baseYscale = 1.0
	var baseMaxSpeed:CGFloat = 400.0

	// Sprites / Textures
	var walkSprites = [SKTexture]()
	var pushSprites = [SKTexture]()
	var pullSprites = [SKTexture]()
	var jumpSprite = SKTexture()
	var stopSprite = SKTexture()
	var holdSprite = SKTexture()
	var pushSprite = SKTexture()
	var pullSprite = SKTexture()
	var attackSprite = SKTexture()

	
	required init?(coder aDecoder: NSCoder){
		super.init(coder: aDecoder)

		let animationComponent = AnimationComponent(forPanda: self)
		self.entity?.addComponent(animationComponent)

		setPanda("Filhote")
	}
	
	func setPanda(_ type: String){
		who = type
		walkSprites = [ SKTexture(imageNamed: "\(type)Andando1"), SKTexture(imageNamed: "\(type)Andando2") ]
		holdSprite = SKTexture(imageNamed: "\(type)Segurando")
		stopSprite = SKTexture(imageNamed: "\(type)Andando1")
		jumpSprite = SKTexture(imageNamed: "\(type)Pulando")
		pushSprite = SKTexture(imageNamed: "\(type)Empurrando1")
		pullSprite = SKTexture(imageNamed: "\(type)Puxando1")
		pushSprites = [ pushSprite, SKTexture(imageNamed: "\(type)Empurrando2")]
		pullSprites = [ pullSprite, SKTexture(imageNamed: "\(type)Puxando2")]

		if type=="Filhote" {
			//set the physics for the child
			walkInterval = 0.1
			baseWalkForce = 20000.0
			baseJumpStrength = 20 * 1000.0
			baseMass = 20.0
			baseMassHigh = 80.0
			baseLinearDamping = 25
			baseFriction = 0.1
			baseXscale = 0.25
			baseYscale = 0.25
		} else if type=="Mae" {
			//set the physics for the mother
			walkInterval = 0.1
			baseWalkForce = 200000.0
			baseJumpStrength = 80 * 1000.0
			baseMass = 70.0
			baseMassHigh = 2000.0
			baseLinearDamping = 80
			baseFriction = 0.2
			baseXscale = 0.5
			baseYscale = 0.5
		}
		self.xScale = CGFloat( baseXscale )
		self.yScale = CGFloat( baseYscale )
	}
	
	func walkBy(_ direction: Double){
		guard let pb = physicsBody else { return }
		
		// MOVE YOUR BODYYYYYYYYYY
		isWalking = true
		let walkSpeed = pb.velocity.dx
		let walkForce = CGVector(dx: direction * baseWalkForce, dy: 0.0)

		if abs(walkSpeed)<self.baseMaxSpeed { pb.applyForce(walkForce) }
		
		if isJumping { return }
		if abs(pb.velocity.dx) > 10.0 {
			walkAnimate()

			if !isHolding {
//				if direction > 0.0 { lookForward() }
//				else { lookBack() }
			}
		}
	}
	
	func jump(){
		guard let pb = physicsBody else { return }
		
//		print( who )
//		print( pb.mass )
//		print( pb.linearDamping )
//		print( pb.friction )
//		print( baseJumpStrength )
		
//		if ( isGrounded ) {
			pb.applyImpulse(CGVector(dx: 0, dy: baseJumpStrength))
			isJumping = true
			isGrounded = false
//		}
        if self.who == "Filhote" {
            SoundManager.main.play(sound: .jumpCub)
        } else {
            SoundManager.main.play(sound: .jumpMom)
        }
	}
	
	func checkAir(){
		guard let pb = physicsBody else { return }

		isGrounded = false
		for touching in pb.allContactedBodies() {
			if touching.categoryBitMask == 42 {
				isGrounded = true
			}
		}

		let delta = abs( lastPosition.y - position.y )
		if delta>2.0 {
			self.removeAction(forKey: "pandaWalking")
			self.texture = jumpSprite
		} else {
			isJumping = false
			self.texture = isHolding ? holdSprite : stopSprite
			if isGrounded {
				if ( !isWalking ) { self.stop() }
			}
		}
		lastPosition = position
	}

	func stop() {
		guard let pb = physicsBody else { return }
			isWalking = false
		
			walkDirection = 0.0
			pb.applyForce(CGVector(dx: 0.0, dy: 0.0))

			pb.mass = CGFloat( baseMassHigh )
			pb.friction = 1
			if isGrounded { pb.linearDamping = CGFloat( baseLinearDamping ) }

			self.run(SKAction.wait(forDuration: 0.1)){
				pb.mass = CGFloat( self.baseMass )
				pb.friction = CGFloat( self.baseFriction )
				pb.linearDamping = 1
				self.removeAction(forKey: "pandaWalking")
			}
	}
	
	func lookBack(){
		let newScale = -1*abs(self.xScale)
		self.run(SKAction.scaleX(to: newScale, duration: 0.0))
	}
	
	func lookForward(){
		let newScale = abs(self.xScale)
		self.run(SKAction.scaleX(to: newScale, duration: 0.0))
	}
	
	// Generic action functions
	func doAction(){
		if who == "Filhote" { self.hold() }
		else { self.attack() }
	}
	func doDropAction(){
		if who == "Filhote" { self.dropHold() }
	}
	
	// Attack functions
	func attack(){
		
	}

	// Hold functions
	func hold(){
		if !isJumping {
			self.texture = holdSprite
			self.isHolding = true
			self.baseWalkForce = 4 * self.baseWalkForce
			self.baseMaxSpeed = 100
		}
	}
	
	func dropHold(){
		if isHolding {
			self.texture = stopSprite
			self.isHolding = false
			self.baseWalkForce = 0.25 * self.baseWalkForce
			self.baseMaxSpeed = 400
		}
	}
	
	func walkAnimate(){
		let isWalking = self.action(forKey: "pandaWalking")
		if isWalking != nil { return }
		var moveSprites = [SKTexture]()
		if isHolding {
			guard let pb = physicsBody else { return }
			if pb.velocity.dx > 0 {
				moveSprites = self.xScale>0 ? pushSprites : pullSprites
			} else {
				moveSprites = self.xScale>0 ? pullSprites : pushSprites
			}
		} else {
			moveSprites = walkSprites
		}
		let walkActionAnim = SKAction.animate(with: moveSprites, timePerFrame: walkInterval)
		let walkAction = SKAction.repeatForever(SKAction.repeatForever(walkActionAnim))
		self.run(walkAction, withKey: "pandaWalking")
	}
	
	
}




