//
//  AnimationComponent.swift
//  PandorgaV0
//
//  Created by Rafael Avelino do Prado Garcia Ribeiro on 12/11/17.
//  Copyright © 2017 Caio Felipe Giasson. All rights reserved.
//

import SpriteKit
import GameplayKit

class AnimationComponent: GKComponent {
    
    // Identificador da SKAction que anima as textureas
    private let key: String = "pandaWalking"
    
    // Panda a ser animado
    var node: Panda
    
    init(forPanda panda: Panda) {
        node = panda
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Anima as texturas se necessário
    override func update(deltaTime seconds: TimeInterval) {
        super.update(deltaTime: seconds)
        
        // Se estiver andando passa
        guard node.isWalking == true else {
            // Se não estiver, mas ainda estiver animando, encerra a animação
            if node.action(forKey: key) != nil {
                node.removeAction(forKey: key)
            }
            return
        }
        
        let action = SKAction.animate(with: node.walkSprites, timePerFrame: 0.2)
        node.run(.repeatForever(action), withKey: key)
    }
}

