//
//  GameViewController.swift
//  PandorgaV0
//
//  Created by Caio Giasson on 27/10/17.
//  Copyright © 2017 Caio Felipe Giasson. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

		SoundManager.main.play(sound: .intro)

        if let scene = MainScreen(fileNamed: "MainScreen") {
            if let sceneNode = scene as! MainScreen? {
                sceneNode.scaleMode = .aspectFill
				if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    view.ignoresSiblingOrder = false
                    view.showsFPS = false
                    view.showsNodeCount = false
                }
            }
		}
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
