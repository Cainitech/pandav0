//
//  GameScene.swift
//  PandorgaV0
//
//  Created by Caio Giasson on 27/10/17.
//  Copyright © 2017 Caio Felipe Giasson. All rights reserved.
//

import SpriteKit
import GameplayKit

typealias TileCoordinates = (column: Int, row: Int)

class GameScene: SKScene, SKPhysicsContactDelegate {
	
	// TEMPS
	// FIM TEMPS
	
	private var theMap: SKTileMapNode!
	private var thePanda: Panda!

	private var firstTouchPoint = CGPoint()
	private var currentTouchPoint = CGPoint()
	
	// CONTROLS
	var control: Joypad!
	var displacement:Double = 0.0
	var isControlling = false
	var lastCameraPosition:CGFloat = 0.0
	
	// MAP ELEMENTS
	var mapHole1: SKSpriteNode!
	var mapHole2: SKSpriteNode!
	var mapHole3: SKSpriteNode!
	var mapRock1: SKSpriteNode!
	var mapRegionBlock: SKNode!
	var mapRegions = [SKSpriteNode]()
	var mapWalkout: SKSpriteNode!
	var mapIsRockOnHole = false
	var fundoFase: SKSpriteNode!
	
	var mapDetails =  [SKSpriteNode]()

	var lastUpdatedTime: TimeInterval = 0
	
	override func didMove(to view: SKView) {
		physicsWorld.contactDelegate = self
		
		theMap = childNode(withName: "map0") as! SKTileMapNode
		thePanda = childNode(withName: "panda") as! Panda
		thePanda.setPanda("Filhote")

//		self.setupPlatformsPhysics()

		// MAP ELEMENTS
		mapHole1 = childNode(withName: "bigHole1") as! SKSpriteNode
		mapHole2 = childNode(withName: "smallHole1") as! SKSpriteNode
		mapHole3 = childNode(withName: "smallHole2") as! SKSpriteNode
		mapRock1 = childNode(withName: "mapRock1") as! SKSpriteNode
		fundoFase = childNode(withName: "FundoFase") as! SKSpriteNode
		
		mapRegionBlock = childNode(withName: "regions")
		for i in 1...21 {
			let region = childNode(withName: "bigRegion\(i)") as! SKSpriteNode
			mapRegions.append(region)
		}
		mapWalkout = childNode(withName: "mapWalkout") as! SKSpriteNode

		for i in 1...12 {
			let detail = childNode(withName: "details\(i)") as! SKSpriteNode
			mapDetails.append(detail)
		}
		
		control = Joypad(self)

		SoundManager.main.play(sound: .intro)
		
    }

	func tile(in tileMap: SKTileMapNode, at coordinates: TileCoordinates) -> SKTileDefinition? {
		return tileMap.tileDefinition(atColumn: coordinates.column, row: coordinates.row)
	}

	func setupPlatformsPhysics(){
		guard let platforms = theMap else { return }
		
		var physicsBodies = [SKPhysicsBody]()
		
		for row in 0..<platforms.numberOfRows {
			for column in 0..<platforms.numberOfColumns {
				guard let tile = tile(in: platforms, at: (column, row) ) else { continue }
				
				let center = platforms.centerOfTile(atColumn: column, row: row)
				let body = SKPhysicsBody(rectangleOf: tile.size, center: center)
//				if tile.name!.range(of: "chao") != nil { }
				physicsBodies.append(body)
			}
		}
		self.theMap?.physicsBody = SKPhysicsBody(bodies: physicsBodies)
		self.theMap?.physicsBody?.isDynamic = false
		self.theMap?.physicsBody?.restitution = 0
		self.theMap?.physicsBody?.friction = 0
		self.theMap?.physicsBody?.categoryBitMask = 42
	}

	
	func doDirection(_ location: CGPoint, _ firstTouch: Bool){
		if firstTouch {
			firstTouchPoint = location
		} else {
			currentTouchPoint = location
//			calcDisplacement()
		}
	}
	
	func calcDisplacement() {
		let limit = CGFloat(10.0)
		let delta = currentTouchPoint.x - firstTouchPoint.x
		self.displacement = abs(delta) > limit ? Double( sign( Double( delta * limit ) ) ) : Double( delta )
	}
	
	func doAction(_ location: CGPoint, _ began: Bool){
		let largura = self.view?.frame.width
		if location.y < largura!/4 {
			if began {
				thePanda.doAction()
			} else {
				thePanda.doDropAction()
			}
		} else {
			if began { thePanda.jump() }
		}
	}
	
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let largura = self.view?.frame.width
		for touch in touches {
			
			let location = touch.location(in: self.view)
			if location.x < largura!/3 {
				doDirection(location, true)
				isControlling = true
			}
			else if location.x > 2*largura!/3 {
				doAction(location, true)
			}
			
			let loc2 = touch.location(in: self)
			if location.x < largura!/3 {
				control.show()
				control.setStart(loc2)
			}
		
		}
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		let largura = self.view?.frame.width
		for touch in touches {
			let location = touch.location(in: self.view)
			if location.x < largura!/3 {
				isControlling = true
				doDirection(location, false)
			} else {
//				if isControlling {
//					thePanda.stop()
//					control.hide()
//					isControlling = false
//				}
			}
			
			let loc2 = touch.location(in: self)
			if location.x < largura!/3 {
				control.moveCtrl(loc2 )
			}
		}
	}
	
	func doResetGame(){
		thePanda.position = CGPoint(x: -380.0, y: -40.0)
	}
	
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		let largura = self.view?.frame.width
		
		for touch in touches {
			let location = touch.location(in: self.view)
			if location.x < largura!/3 {
				thePanda.stop()
				control.hide()
			}
			else {
				doAction(location, false)
			}
		}
		
    }
	
	func doZoom(_ level: Int, _ posY: Int){
		guard let camera = camera else { return }
		
		let inLevel = Double(level)
		let camPosition = CGFloat(posY)
		let zoomLevel = CGFloat(0.8 + 0.2*inLevel)
		camera.run( SKAction.moveTo(y: camPosition, duration: 1) )
		camera.run( SKAction.scale(to: zoomLevel, duration: 1) )
	}
	
	func checkPosition(){

		// Verificando se caiu no buraco
		if mapHole1.contains( thePanda.position ) {
			doResetGame()
		}
		
		// Verificando se a rocha está no buraco, liberando a passagem do primeiro animal
		if mapHole2.contains( mapRock1.position ) {
			mapIsRockOnHole = true
		}
		
		// Avaliando o zoom
		if mapHole3.contains(thePanda.position) {
			doZoom(2, 1700)
		} else
		if mapRegions[0].contains(thePanda.position) {
			doZoom(1, 0)
		} else
		if mapRegions[1].contains(thePanda.position) {
			doZoom(2, 300)
//			thePanda.setPanda("Mae")
		} else
		if mapRegions[2].contains(thePanda.position) {
			doZoom(3, 600)
		} else
		if mapRegions[3].contains(thePanda.position) {
			doZoom(2, 300)
		} else
		if mapRegions[4].contains(thePanda.position) {
			doZoom(3, 800)
		} else
		if mapRegions[5].contains(thePanda.position) {
			doZoom(3, 1400)
		} else
		if mapRegions[6].contains(thePanda.position) {
			doZoom(3, 1900)
		} else
		if mapRegions[7].contains(thePanda.position) {
			doZoom(4, 2200)
		} else
		if mapRegions[8].contains(thePanda.position) {
			doZoom(4, 2200)
		} else
		if mapRegions[9].contains(thePanda.position) {
			doZoom(3, 300)
		} else
		if mapRegions[10].contains(thePanda.position) {
			doZoom(3, 600)
		} else
		if mapRegions[11].contains(thePanda.position) {
			doZoom(4, 900)
		} else
		if mapRegions[12].contains(thePanda.position) {
			doZoom(3, 1100)
		} else
		if mapRegions[13].contains(thePanda.position) {
			doZoom(3, 300)
		} else
		if mapRegions[14].contains(thePanda.position) {
			doZoom(2, 2100)
		} else
		if mapRegions[15].contains(thePanda.position) {
			doZoom(2, 200)
		} else
		if mapRegions[16].contains(thePanda.position) {
			doZoom(2, 2200)
		} else
		if mapRegions[17].contains(thePanda.position) {
			doZoom(2, 1900)
		} else
		if mapRegions[18].contains(thePanda.position) {
			doZoom(2, 1660)
		} else
		if mapRegions[19].contains(thePanda.position) {
			doZoom(2, 1200)
		} else
		if mapRegions[20].contains(thePanda.position) {
			doZoom(2, 500)
		} else
		if mapWalkout.contains(thePanda.position) {
			doZoom(1, 400)
			if thePanda.who == "Filhote" {
				doChangeToMotherPhase()
			} else {
				if let scene = Credits(fileNamed: "Credits") {
					if let sceneNode = scene as! Credits? {
						let transition = SKTransition.crossFade(withDuration: 0.5)
						sceneNode.scaleMode = .aspectFill
						if let view = self.view as! SKView? {
							view.presentScene(sceneNode, transition: transition)
							view.ignoresSiblingOrder = false
							view.showsFPS = false
							view.showsNodeCount = false
						}
					}
				}

			}
		}
//		} else
//		if mapRegions[11].contains(thePanda.position) {
//			doZoom(3, 600)
//		} else
//			// PASSAGEM DE FASE
//		if mapWalkout.contains(thePanda.position) {
//			doChangeToMotherPhase()
//		}
	}
	
	func doFullReset(){
		guard let camera = camera else { return }
		camera.run( SKAction.move(to: CGPoint(x: 0, y: 0), duration: 2) )

		thePanda.position = CGPoint(x: -380.0, y: -40.0)
		thePanda.setPanda("Filhote")
		thePanda.stop()
		
//		let theColor = SKColor.init(red: 0.698, green: 0.843, blue: 1.0, alpha: 1.0)
//		self.run( SKAction.colorize(with: theColor, colorBlendFactor: 0.4, duration: 2) )
		fundoFase.texture = SKTexture(imageNamed: "Fundo_filhote")

		stopPlayScaryDetails()
	}
	
	func doChangeToMotherPhase(){
		guard let camera = camera else { return }
		camera.run( SKAction.move(to: CGPoint(x: 0, y: 0), duration: 2) )
		
		thePanda.position = CGPoint(x: -380.0, y: -40.0)
		thePanda.setPanda("Mae")
		
//		let theColor = SKColor.init(red: 0.3, green: 0.1, blue: 0.1, alpha: 0.7)
//		self.run( SKAction.colorize(with: theColor, colorBlendFactor: 0.4, duration: 2) )
		fundoFase.texture = SKTexture(imageNamed: "Fundo_mae")
		
		startPlayScaryDetails()
	}
	
	func startPlayScaryDetails(){
		let showHide = SKAction.sequence([
			SKAction.fadeAlpha(to: 0.3, duration: 1),
			SKAction.wait(forDuration: 1),
			SKAction.fadeAlpha(to: 0.0, duration: 1),
			SKAction.wait(forDuration: 4)
		])
		
		for details in mapDetails {
			details.run(SKAction.repeatForever(showHide))
		}
	}
	
	func stopPlayScaryDetails(){
		for details in mapDetails {
			details.removeAllActions()
		}
	}
	
    override func update(_ currentTime: TimeInterval) {
		checkPosition()

		thePanda.walkBy( Double(control.dispX) )
		if control.lastMove >= 0 { thePanda.lookForward() }
		else { thePanda.lookBack() }

		thePanda.checkAir()
		guard let camera = camera else { return }
		lastCameraPosition = camera.position.x
		camera.position.x = thePanda.position.x

		let diffX = lastCameraPosition - camera.position.x
		control.moveXby(diffX)
		
//		let posX = thePanda.position.x - 500.0
//		let posY = -1*firstTouchPoint.y
//		control.moveCtrl(CGPoint(x: posX, y: posY))

//		let dt = currentTime - lastUpdatedTime
		lastUpdatedTime = currentTime

	}
}





